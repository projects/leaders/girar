Name: girar
Version: 0.5
Release: alt1.eter1

Summary: git.eter server engine
License: GPLv2+
Group: System/Servers
Packager: Evgeny Sinelnikov <sin@altlinux.ru>

Source: %name-%version.tar

Requires: quota
Requires: rsync

Requires(pre): shadow-utils
# due to "enable -f /usr/lib/bash/lockf lockf"
Requires: bash-builtin-lockf >= 0:0.2
# due to post-receive hook
Requires: git-core >= 0:1.5.1
# due to girar-task-add
Requires: gear
# due to gb-sh-rpmhdrcache
Requires: memcached rpmhdrmemcache
# due to cron jobs
Requires: stmpclean
# due to "locale -m"
Requires: glibc-i18ndata
# due to gb-task-repo-unmets
Requires: qa-robot >= 0.3.4-alt1

Obsoletes: girar-builder

BuildRequires: perl(RPM.pm) perl(Date/Format.pm)

%define girar_admin girar-admin
%define users_group girar-users

%define girar_keydir %_prefix/lib/%name-gpgkeys

%description
This package contains server engine initially developed for git.eter,
including administration and user utilities, git hooks, email
subscription support and config files. %packager

%prep
%setup

%build
%make_build

%install
%makeinstall_std
echo 0 >%buildroot/var/lib/girar/tasks/.max-task-id
mksock %buildroot/var/run/girar/{acl,depot,repo}/socket
mkdir -p %buildroot/var/spool/cron
touch %buildroot/var/spool/cron/{bull,cow}

mkdir -p %buildroot/usr/libexec/girar-builder
cp -a gb/gb-* gb/remote gb/template %buildroot/usr/libexec/girar-builder/
%add_findreq_skiplist /usr/libexec/girar-builder/remote/*

touch %buildroot%_localstatedir/%name/people-packages-list

mkdir -p %buildroot%girar_keydir
cat >%buildroot%girar_keydir/gpg.conf <<"_EOF"
no-greeting
lock-never
always-trust
no-secmem-warning
quiet
_EOF

cat >%buildroot%_sysconfdir/%name/canonical.regexp <<"_EOF"
/^git_(.+)@/      ${1}@etersoft.ru
_EOF

#mkdir -p %buildroot%_spooldir/%name/tasks/index
#mkdir -p %buildroot%_spooldir/%name/tasks/archive
#mkdir -p %buildroot%_spooldir/%name/tasks/archive/done

%check
cd gb/tests
./run

%pre
%_sbindir/groupadd -r -f girar
%_sbindir/groupadd -r -f girar-users
%_sbindir/groupadd -r -f girar-admin
%_sbindir/groupadd -r -f tasks
#/usr/sbin/useradd -r -g girar -d %_spooldir/%name -s /dev/null -c 'The girar spool processor' -n girar >/dev/null 2>&1 ||:
#/usr/sbin/useradd -r -g girar-builder -d /dev/null -s /dev/null -c 'The girar build processor' -n girar-builder >/dev/null 2>&1 ||:
for u in acl depot repo; do
	%_sbindir/groupadd -r -f $u
	%_sbindir/useradd -r -g $u -G girar -d /var/empty -s /dev/null -c 'Girar $u robot' -n $u ||:
done
for u in bull cow; do
	%_sbindir/groupadd -r -f $u
	%_sbindir/useradd -r -g $u -G girar,tasks -d /var/lib/girar/$u -c "Girar $u robot" -n $u ||:
done

%post
%post_service girar-proxyd-acl
%post_service girar-proxyd-depot
%post_service girar-proxyd-repo
%_sbindir/girar-make-template-repos
if [ $1 -eq 1 ]; then
	if grep -Fxqs 'EXTRAOPTIONS=' /etc/sysconfig/memcached; then
		sed -i 's/^EXTRAOPTIONS=$/&"-m 2048"/' /etc/sysconfig/memcached
	fi
	if grep -Fxqs 'AllowGroups wheel users' /etc/openssh/sshd_config; then
		sed -i 's/^AllowGroups wheel users/& girar-users/' /etc/openssh/sshd_config
	fi
	crontab -u bull - <<-'EOF'
	#1	*	*	*	*	/usr/libexec/girar-builder/gb-toplevel-commit sisyphus
	EOF
	crontab -u cow - <<-'EOF'
	#1	*	*	*	*	/usr/libexec/girar-builder/gb-toplevel-build sisyphus
	40	7	*	*	*	/usr/sbin/stmpclean -t 14d $HOME/.cache
	EOF
fi

%preun
%preun_service girar-proxyd-acl
%preun_service girar-proxyd-depot
%preun_service girar-proxyd-repo

%files
%config(noreplace) %attr(400,root,root) /etc/sudoers.d/girar
%config(noreplace) %attr(640,root,girar) /etc/%name/subscription
%config(noreplace) /etc/sisyphus_check/check.d/*
/etc/girar/
/usr/libexec/girar/
/usr/libexec/girar-builder/
%_datadir/girar/
%_initdir/girar-proxyd-*
%attr(700,root,root) %_sbindir/*

%doc LICENSE TASK gb/conf/

# all the rest should be listed explicitly
%defattr(0,0,0,0)

%config(noreplace) %attr(664,root,girar) %_localstatedir/%name/people-packages-list
%dir %attr(0750,root,%users_group) %girar_keydir
%attr(0750,root,%users_group) %girar_keydir/gpg.conf

%dir %attr(755,root,root) /var/lib/girar/
%dir %attr(2775,root,acl) /var/lib/girar/acl/
%dir %attr(770,root,bull) /var/lib/girar/bull/
%dir %attr(770,root,cow) /var/lib/girar/cow/
%dir %attr(770,root,cow) /var/lib/girar/cow/.cache/
%dir %attr(755,root,root) /var/lib/girar/depot/
%dir %attr(770,root,depot) /var/lib/girar/depot/.tmp/
%dir %attr(775,root,depot) /var/lib/girar/depot/??/
%dir %attr(755,root,root) /var/lib/girar/repo/
%dir %attr(755,root,root) /var/lib/girar/people/
%dir %attr(775,root,bull) /var/lib/girar/gears/
%dir %attr(775,root,bull) /var/lib/girar/srpms/

%dir %attr(3775,bull,tasks) /var/lib/girar/tasks/
%dir %attr(3775,root,bull) /var/lib/girar/tasks/archive/
%dir %attr(775,root,bull) /var/lib/girar/tasks/archive/*
%dir %attr(755,root,root) /var/lib/girar/tasks/index/
%config(noreplace) %attr(664,bull,tasks) /var/lib/girar/tasks/.max-task-id

%dir %attr(750,root,girar) /var/lib/girar/email/
%dir %attr(755,root,root) /var/lib/girar/email/*

%dir %attr(750,root,girar) /var/lock/girar/
%dir %attr(770,root,bull) /var/lock/girar/bull/
%dir %attr(770,root,cow) /var/lock/girar/cow/

%dir %attr(750,root,girar) /var/run/girar/
%dir %attr(710,root,girar) /var/run/girar/acl/
%dir %attr(710,root,bull) /var/run/girar/depot/
%dir %attr(710,root,bull) /var/run/girar/repo/
%ghost %attr(666,root,root) /var/run/girar/*/socket

%config(noreplace) %ghost %attr(600,bull,crontab) /var/spool/cron/bull
%config(noreplace) %ghost %attr(600,cow,crontab) /var/spool/cron/cow

%changelog
* Wed Jan 23 2013 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter45
- commit for build with etersoft autorelease
- girar-autorelease: fix for releases with '_' suffix.

* Wed Dec 19 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter44
- post-update: fix recent_committer permission problem with utimensat().

* Wed Nov 21 2012 Dmitry V. Levin <ldv@altlinux.org> 0.5-alt1
- Imported girar-builder.

* Fri Nov 16 2012 Dmitry V. Levin <ldv@altlinux.org> 0.4-alt1
- Imported gb-depot.

* Wed Jul 18 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter43
- girar-task-add, girar-copy: fix copy actions for pockets support.
- girar-task-run: add pocket support for del and copy actions.
- girar-task-add: fix check for kmod operation.
- girar-pocket-create: try workaround for pocket directory on sshfs.
- Fix kernel modules build to pockets and coping from pockets.
- girar-normalize-repo-name: fix escaped variable spaces.
- girar-quota: silence quota's stderr (thx Dmitry V. Levin).
- girar-add: create private, etc and incoming subdirs owned by
  group girar-admin (thx Dmitry V. Levin).
- girar-task-run: create "arepo" subdir (thx Dmitry V. Levin).
- hooks/post-receive: new hook (thx Dmitry V. Levin).

* Sun Jan 22 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter42
- girar-task-add: fix check for branch and genbases.

* Wed Jan 18 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter41
- Enhance error diagnostics (ALT#26803) (thx Dmitry V. Levin).
- girar-build: add --deps (thx Dmitry V. Levin).
- girar-task-deps: disallow reflexive dependencies (thx Dmitry V. Levin).
- Use girar-sh-tmpdir (thx Dmitry V. Levin).
- girar-sh-tmpdir: new helper (thx Dmitry V. Levin).
- girar-task-depends: new command (thx Dmitry V. Levin).
- Add new state: POSTPONED (thx Dmitry V. Levin).
- girar-task-abort (do_abort): be more verbose (thx Dmitry V. Levin).
- girar-task-ls: add --brief option (thx Dmitry V. Levin).
- girar-task-show, girar-task-ls: show swift flag (thx Dmitry V. Levin).
- girar-task-run: check copy, del and srpm subtasks for validity (thx Dmitry V. Levin).
- girar-task-run: add --dry-run option (thx Dmitry V. Levin).
- girar-task-add srpm: use girar-check-nevr-in-repo (thx Dmitry V. Levin).
- New utility (thx Dmitry V. Levin).
- girar-task-add: add srpm support to check_already_added() and
  its subordinates (thx Dmitry V. Levin).
- girar-task-add srpm: decode and save the NEVR (thx Dmitry V. Levin).
- girar-task-make-index-html: use temporary file for time consuming
  operations (thx Dmitry V. Levin).
- girar-task-abort: new command (thx Dmitry V. Levin).
- girar-task-check-git-inheritance: new command (thx Dmitry V. Levin).
- girar-copy: Add copy command for simplify coping.

* Fri Sep 23 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter40
- lib/rsync.c: wrap getcwd to avoid information disclosure (thx Dmitry V. Levin).
- girar-acl-apply-changes: drop NMU support (thx Dmitry V. Levin).
- girar-acl: drop NMU support (thx Dmitry V. Levin).
- girar-check-perms: drop ACL NMU support (thx Dmitry V. Levin).
- Correct pkg_version at get_package_info().
- Add default GB_TASKS_DONE_DIR.

* Tue Aug 02 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter39
- Add initial admin scripts.
- Pack /tasks/archive and /tasks/index directories
- Add template canonical.regexp for default git users emails
- Create default gpg.conf for /usr/lib/girar-gpgkeys
- girar-task-add: fix gear repo directory for kernel module.
- girar-kmod-autorelease: improve error handling for gear-create-tag.
- Replace girar-stamp-spec code for autorelease logic.
- Fix girar-task-add for genbases command without arguments.

* Sun Jul 17 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter38
- girar-kmod-autorelease: fix kernel modules autorelease for
  sisyphus-check rules.
- girar-kmod: add new complex command for kernel modules building.
- girar-task-add: change argument order for kernel modules build.
- girar-task-show: Update for kernel module build subtasks.
- Fix help message for girar-autorelease.
- Add support for building kernel modules.
- girar-task-ls: show pocket for repos if exist.
- girar-task-add: pack heads and tags for efficient repository access (thx Dmitry V. Levin).
- girar-task-ls: add EPERM state (thx Dmitry V. Levin).
- girar-task-run: allow to run a EPERM task if it passes acl check (thx Dmitry V. Levin).
- girar-task-run: check permissions after all other checks (thx Dmitry V. Levin).
- girar-task-run: check owner match before checking superuser (thx Dmitry V. Levin).
- girar-task-run: fetch task owner information earlier (thx Dmitry V. Levin).
- Add EPERM state (thx Dmitry V. Levin).

* Wed Jun 08 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter37
- Fix error handling for girar-task-ls

* Mon Apr 11 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter36
- Merge with last ALT Linux update (thx Dmitry V. Levin)
+ Update for task/seq -> task/state change
+ Adding or removing a subtask makes the task NEW
+ Introduce girar-task-change-state
+ Introduce girar-task-update-queues
+ girar-task-*:
   take into account that subtasks numbers are now octal
+ girar-task-ls:
   rewrite using $GB_TASKS/index
+ girar-task-show:
   fix fresh typo
+ girar-task-run:
   create a directory to cache results of install check
   relax permissions for parallelized model
   tighten permissions back
   put TESTED tasks to the PENDING queue
   relax permissions a bit again for renaming subtasks
   always place test-only tasks to the AWAITING queue
   add repo name to the log message
+ girar-task-add:
   fix fresh typo
   assign to subtasks octal numbers with step 0100
   implement insert operation
+ check_task_modifiable:
   PENDING is now a modifiable state
+ girar-task-change-state:
   call girar-task-make-index-html before girar-task-update-queues
+ girar-make-task-index-html:
   show test-only flag for TESTED tasks as well

* Tue Mar 15 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter35
- Add global email subscription to find-subscribers. Global configuration saves
  in /etc/girar/subscription with same syntax as user email-distribution

* Thu Mar 03 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter34
- Fix umask for /projects to 0002 for git-receive-pack command

* Mon Jan 24 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter33
- Set umask to 0022 for git-upload-pack in /projects
- Add support for various default prefix of repos
- girar-autorelease: spec file eval with fixed SYSARCH

* Thu Dec 16 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter32
- Fixed girar-get-repo-suffix with empty GIRAR_REPO_SUFFIX again

* Fri Nov 26 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter31
- Fixed girar-get-repo-suffix with empty GIRAR_REPO_SUFFIX

* Mon Nov 22 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter30
- Add backports support for autorelease build

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter29
- Fix using src.list for pockets
- Use default settings for Girar Packager name and email

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter28
- Clean code from auto_release flag due it unused
- Add girar-rebuild for branch building
- Fixed autorelease support

* Mon Nov 08 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter27
- Add autorelease support for branch using with girar-task-add

* Mon Oct 18 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter26
- Add support for building from projects
- Add '--auto-release' option for package build running

* Mon Oct 18 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter25
- Revert people-packages-list file
- Fix packages list cache for /projects repositories

* Mon Oct 18 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter24
- Remove obsletes cron file and simple service for old acl merging scheme
- Revert localstatedir directories for package

* Mon Oct 18 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter23
- girar-ls: for pockets directory
- Merge with last update
+ girar-check-acl-leader:
   Fix typos (ALT#24096)
+ girar-task-add:
   add more verbosity
   girar-task-add: create pkg.tar from gear repository
+ girar-task-*:
   use girar-make-task-index-html
+ girar-make-task-index-html:
   new utility to make index.html for the specified task
+ girar-acl-proxyd:
   Implement girar acl proxy daemon
   Drop check for GB_USER's uid
+ girar-task-rm:
   fix test-only check
+ Remove obsolete UPRAVDOM_* variables
+ girar-acl-notify-changes:
  include the name of author into report
+ girar-acl-merge-changes:
   new utility
   do not send dumb email report to author of successfully completed acl change request
   implement --quiet option
+ girar-acl:
   close temporary file descriptor
   implement --quiet option
   remove obsolete utility
   redirect ACL commands to /var/spool/girar/acl/socket
   empty tmp file before reuse
   robustify command channel
   obtain a shared lock on $GIRAR_ACL_CONF_DIR
   remove redundant checks
   use girar-acl-apply-changes girar-get-email-address
   use girar-acl-show utility
+ girar-merge-acl:
   use girar-acl-apply-changes
   more code reuse
   use girar-acl-notify-changes and
+ Drop obsolete GIRAR_ACL_STATE_DIR
+ Package /var/spool/girar/acl/
+ girar-acl-apply-changes:
   validate the repository name specified on command line
   enhance "delete" handling
+ girar-connect-stdout:
   new utility
+ girar-acl-apply-changes:
   new utility, factored out from girar-merge-acl
+ girar-acl-show:
   take additional optional argument
+ girar-acl-notify-changes:
   new utility, factored out from girar-merge-acl
+ girar-get-email-address:
   new utility, factored out from girar-merge-acl
+ Use quote_sed_regexp_variable() instead of quote_sed_regexp()
+ spec: assign %_spooldir/%name/tasks directory to %gb_user
+ girar-task-delsub:
   Allow to delete first subtask

* Tue Jul 13 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter22
- Add support for projects type email notification

* Sun Jun 13 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter21
- Merge with last update (thx Dmitry V. Levin)
+ girar-acl-show: Fix acl file check
+ task run: Implement --test-only option
+ girar.spec: Create girar-builder user as well

* Fri Apr 02 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter20
- Pack acl.pub directory

* Wed Mar 31 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter19
- Add girar-run command for spooler using with cron
- Add cron.d example

* Sun Feb 28 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter18
- Fix set_repo using for girar-task-new.
- Merge with last update (thx Dmitry V. Levin)
+ girar-archive-tasks:
   Rename to gb-archive-stale-tasks and move to girar-builder
+ girar-task-ls, girar-task-show:
   display test only tasks
+ Log task creation, run and removal; subtask addition and deletion
+ girar-merge-acl:
   Merge reports for @nobody and @everybody
   Do not exclude @everybody, @nobody and @qa from notification service
   Send email notifications to all engaged people (ALT#19351)
   Add transaction completion status to the summary
   When updating ACL, treat @qa the same way as @everybody (ALT#22702)
+ girar-archive-tasks: Be verbose about tasks being archived
+ girar-task-add:
   Make cloned git repository group writable
   Cloned git repository is going to be tweaked by the builder later.
   Create all git directories writable by builder
+ sbin/.gitignore: Update
+ girar-task-ls: Add --done option
+ girar-task-rm: Only task owner can remove a task
+ girar-task-new:
   Make the task directory and its task subdirectory group writable and sticky bit protected
+ girar-task-share:
   Provide better error diagnostics
   No need to obtain a lock just to probe for the status
+ girar-task-approve: New keyword "all" approves all available subtasks
+ girar-task-run: Change blocking lock to immediately acquired
+ girar-build:
   Update usage text
   Add del support
   Add copy support
+ Package /etc/girar/repo
+ girar-check-perms: Make handling of unlisted packages configurable
+ girar-check-acl-leader: Check for per-repository superuser
+ girar-check-superuser: New check for per-repository superuser
+ girar-sh-config: Source per-repository config
+ Makefile: Create directory for per-repository configs
+ Cleanup use of cd
+ Check and normalize binary repository name consistently
+ Rename GIRAR_REPOSITORIES -> GIRAR_REPO_LIST
+ girar-task-add (validate_tag_name): Enhance error diagnostics

* Mon Oct 19 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter17
- Fix database movinfg for projects with directories

* Mon Oct 19 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter16
- Add support for directories in /projects root

* Wed Sep 30 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter15
- Set vendor to packages email
- Merge with last update
+ girar-task-add (validate_tag_name): Enhance error diagnostics
+ girar-acl (parse_cmd): Fix args check for root
+ girar-merge-acl: @everybody alone is @nobody
+ girar-merge-acl: Drop support for obsolete #nobody/#all syntax
+ girar-task-delsub: Use gear_nums(), check that gears/$i is writable
  before starting removal

* Tue Aug 25 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter14
- Create gears and plan directories for pockets

* Tue Aug 25 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter13
- Initial pockets support

* Tue Aug 04 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter12
- Add genbases command for simple regenerate apt bases

* Mon Aug 03 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter11
- Add girar spool service

* Sun Jul 05 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter10
- Merge with last update
- Add "repack" command (ALT#18847) (thx Dmitry V. Levin).
- Add default-branch command (ALT#19083) (thx Dmitry V. Levin).
- girar-check-perms: Fix check for random builders (thx Dmitry V. Levin)

* Wed Jun 17 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter9
- Set core.sharedRepository to true for projects.
- fix girar-add for two symbols logins.
- Add update packages list for searching.

* Wed May 27 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter8
- Fixed simple bug at girar-clone for /projects.
- Add rsync to requirements
- Merge with last update
+ girar-task-share: Enhance error diagnostics (thx Dmitry V. Levin).
+ girar-task-add: Remove girar-check-perms call for srpm (thx Dmitry V. Levin).
+ girar-archive-tasks: Tasks archiver (thx Dmitry V. Levin).
+ girar-task-show: non-verbose mode (thx Alexey I. Froloff).
+ girar-task-add: Fix srpm validation (thx Dmitry V. Levin).
+ girar-build: Add srpm support (thx Dmitry V. Levin).
+ girar-task-add (usage): Document srpm (thx Dmitry V. Levin).
+ Implement rsync to incoming support (thx Dmitry V. Levin).
+ sbin/girar-add: Use locked incoming/tmp directory for srpm files (thx Dmitry V. Levin).
+ sbin/girar-add: Create incoming directory (thx Dmitry V. Levin).
+ girar-task-add: Change srpm directory to $HOME/incoming (thx Dmitry V. Levin).
+ girar-task-add (validate_srpm): Run sisyphus_check (thx Dmitry V. Levin).

* Wed May 13 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter7
- Add /projects support

* Tue May 12 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter6
- add config-db command for repack disable.
- Merge with last update
+ girar-task-show: Show information about approval (thx Dmitry V. Levin).
+ girar-task-add: Disallow package copying to sisyphus (thx Dmitry V. Levin).
+ girar-task-add (check_already_added_repo): Enhance errors
  diagnostics (thx Dmitry V. Levin).
+ girar-acl (parse_cmd): Fix nmu show for non-owners (ALT#19839) (thx Dmitry V. Levin).

* Wed Apr 15 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter5
- Merge with last update
+ girar-task-run: Create bugmail toplevel directory
+ girar-sh.c (shell): Handle "--help" the same way as "help" (ALT#19402)
+ girar-acl (parse_cmd), girar-merge-acl (process_user_rules):
  Allow person to del self from the list (ALT#19215)

* Sat Mar 28 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter4
- Merge with last update
+ girar-add: Fix NAME check
+ girar-task-run: Fix check for empty task

* Fri Mar 13 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter3
- Fixed gpgkey check for etersoft

* Sun Mar 08 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter2
- Merge with last update

* Wed Feb 11 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1.eter1
- Merge with last update

* Thu Dec 11 2008 Dmitry V. Levin <ldv@altlinux.org> 0.3-alt1
- Added task subcommands.

* Tue Oct 28 2008 Evgeny Sinelnikov <sin@altlinux.ru> 0.2-alt1.eter1
- Merge with last update

* Mon Jun 16 2008 Dmitry V. Levin <ldv@altlinux.org> 0.2-alt1
- Rewrote hooks using post-receive.

* Thu May 22 2008 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter3
- Merge with last update
  + Add /archive support

* Mon Mar 31 2008 Evgeny Sinelnikov <sin@etersoft.ru> 0.1-alt1.eter2
- Merge with last update

* Fri Feb 22 2008 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter1
- Migrate to etersoft build.

* Tue Nov 21 2006 Dmitry V. Levin <ldv@altlinux.org> 0.1-alt1
- Specfile cleanup.

* Fri Nov 17 2006 Alexey Gladkov <legion@altlinux.ru> 0.0.1-alt1
- Initial revision.
